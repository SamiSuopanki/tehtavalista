<?php 
$db = null;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/cs.css" type="text/css">
        <title>Tehtävälista</title>
    </head>
    <body>
        <div id="content">
        <?php
        try {
            $db = new PDO('mysql:host=localhost;dbname=tehtlistaa;charset-utf-8','root','');
            $db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $pdoex) {
            print "<p>Tietokannan avaus epäonnistui. " . $pdoex->getMessage() . "</p>";
        }
        ?>